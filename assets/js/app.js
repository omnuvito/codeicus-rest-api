import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import ItemCard from './Components/Flavour';

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            entries: []
        };
    }

    componentDidMount() {
        fetch('/api/flavours')
            .then(response => response.json())
            .then(entries => {
                this.setState({
                    entries
                });
            });
    }

    render() {
        return (
            <MuiThemeProvider>
                <div style={{ display: 'flex' }}>
                    {this.state.entries.map(
                        ({ id, name, letter, price }) => (
                            <ItemCard
                                key={id}
                                name={name}
                                letter={letter}
                                style={{ flex: 1, margin: 5 }}
                            >
                                {price}
                            </ItemCard>
                        )
                    )}
                </div>
            </MuiThemeProvider>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));