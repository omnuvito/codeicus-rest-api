import React from 'react';
import { Card, CardHeader, CardTitle, CardText } from 'material-ui/Card';

const ItemCard = ({ name, letter, price, style }) => (
<Card style={style}>
    <CardHeader title={letter} />
    <CardTitle title={name} subtitle={name} />
    <CardText>{price}</CardText>
    </Card>
);

export default ItemCard;