<?php

namespace App\Repository;

use App\Entity\Flavour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Flavour|null find($id, $lockMode = null, $lockVersion = null)
 * @method Flavour|null findOneBy(array $criteria, array $orderBy = null)
 * @method Flavour[]    findAll()
 * @method Flavour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlavourRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Flavour::class);
    }

//    /**
//     * @return Flavour[] Returns an array of Flavour objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Flavour
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
