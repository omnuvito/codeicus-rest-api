<?php

namespace App\Controller\Rest;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PricesController
 * @package App\Controller\Rest
 */
class PricesController extends FOSRestController
{
    /**
     * Get Box Total Price
     *
     * @Rest\Get("/total_price")
     * @return View
     */
    public function getTotalPrice(): View
    {
        //TODO: Call Model to Calculate Box total price
        $price = 0.0;
        return View::create($price, Response::HTTP_OK , []);
    }
}