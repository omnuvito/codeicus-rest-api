<?php

namespace App\Controller\Rest;

use App\Entity\Flavour;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FlavourController
 * @package App\Controller\Rest
 */
class FlavourController extends FOSRestController
{
    /**
     * @var \App\Repository\FlavourRepository
     */
    protected $flavoursRepository;

    /**
     * FlavourController constructor
     *
     * @param \App\Repository\FlavourRepository $flavoursRepository
     */
    public function __construct(
        \App\Repository\FlavourRepository $flavoursRepository
    )
    {
        $this->flavoursRepository = $flavoursRepository;
    }

    /**
     * Get All Flavour
     *
     * @Rest\Get("/flavours")
     * @return View
     */
    public function getAllFlavours(): View
    {
        try {
            $flavours = $this->flavoursRepository->findAll();

            return View::create($flavours, Response::HTTP_OK , []);
        } catch (\Exception $e) {
            return View::create($e->getMessage(), Response::HTTP_OK , []);
        }

    }

    /**
     * Create Flavour
     *
     * @Rest\Post("/flavours")
     * @param Request $request
     * @return Response
     */
    public function createFlavour(Request $request): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $flavour = new Flavour();

        $flavour->setName($request->get('name'));
        $flavour->setLetter($request->get('letter'));
        $flavour->setPrice($request->get('price'));

        try {
            $entityManager->persist($flavour);
            $entityManager->flush();

            $message = 'Saved new flavour with id: ' . $flavour->getId();
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new Response($message);
    }
}
