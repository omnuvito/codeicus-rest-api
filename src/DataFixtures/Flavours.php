<?php

namespace App\DataFixtures;

use App\Entity\Flavour;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Flavours extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $values = [
            [
                'name' => 'Chocolate',
                'letter' => 'C',
                'price' => 1.20
            ],
            [
                'name' => 'Dulce de leche',
                'letter' => 'D',
                'price' => 1.10
            ],
            [
                'name' => 'Frutilla',
                'letter' => 'F',
                'price' => 0.80
            ],
            [
                'name' => 'Limón',
                'letter' => 'L',
                'price' => 0.70
            ],
            [
                'name' => 'Merengue',
                'letter' => 'M',
                'price' => 2.05
            ],
            [
                'name' => 'Nueces',
                'letter' => 'N',
                'price' => 2.85
            ]
        ];

        foreach ($values as $value) {
            $flavour = new Flavour();

            $flavour->setName($value['name']);
            $flavour->setLetter($value['letter']);
            $flavour->setPrice($value['price']);

            $manager->persist($flavour);
            $manager->flush();
        }
    }
}
